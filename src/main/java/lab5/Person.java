/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab5;

/**
 *
 * @author mitpa
 */
public class Person 
{
     public enum Weight {K , LB};
    public enum Height {IN , CM};
    
    //declaring variables
    private String name;
    private double weight;
    private double height;
    
    //constructor 
    public Person(String name,double weight, double height)
    {
        this.name = name;
        this.weight = weight;
        this.height = height;
    }
    
    //getter setter
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public double getWeight()
    {
        return this.weight;
    }
    public void setWeight(double weight)
    {
        this.weight = weight;
    }
    public double getHeight()
    {
        return this.height;
    }
    public void setHeight(double height)
    {
        this.height = height;
    }
public double getWeightLbs()
    {
        return this.weight / 0.454;
    }

public double getHeightInch()
    {
        return this.height/0.025;
    }

    public void BMI()
    {
        double BMI = weight / (height * height);
    }
    
}
